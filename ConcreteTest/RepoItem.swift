//
//  RepoItem.swift
//  ConcreteTest
//
//  Created by Bohm Soluções on 06/07/17.
//  Copyright © 2017 Breno Morais. All rights reserved.
//

import UIKit

class RepoItem: NSObject {
//    Cada item da lista deve exibir Nome / Foto do autor do PR, Título do PR, Data do PR e Body do PR
//    Ao tocar em um item, deve abrir no browser a página do Pull Request em questão
    var rName:String = ""
    var rImage:String = ""
    var rtitle:String = ""
    var rDate:String = ""
    var rBody:String = ""
    var rPullLink:String = ""
}
