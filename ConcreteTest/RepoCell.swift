//
//  RepoCell.swift
//  ConcreteTest
//
//  Created by Breno Morais on 05/07/17.
//  Copyright © 2017 Breno Morais. All rights reserved.
//

import UIKit

class RepoCell: UITableViewCell {
    @IBOutlet weak var rName: UILabel!
    @IBOutlet weak var rDesc: UILabel!
    @IBOutlet weak var rStars: UILabel!
    @IBOutlet weak var rForks: UILabel!
    @IBOutlet weak var rAuthor: UILabel!
    @IBOutlet weak var rAuthorImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        rAuthorImage.image = UIImage.init(named: "placeholder")
    }
    
}
