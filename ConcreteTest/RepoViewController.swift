//
//  RepoViewController.swift
//  ConcreteTest
//
//  Created by Breno Morais on 05/07/17.
//  Copyright © 2017 Breno Morais. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class RepoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var itensTable: UITableView!
    @IBOutlet var openedItens: UILabel!
    @IBOutlet var closedItens: UILabel!
    
    var reposiroty:Repository?
    var itens:[RepoItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Teste iOS"
        loadRepositorieItems()
        
        itensTable.delegate = self
        itensTable.dataSource = self
        itensTable.register(UINib.init(nibName: "RepoItemCell", bundle: Bundle.main), forCellReuseIdentifier: "RepoItemCell")
    }
    
    func loadRepositorieItems(){
        let requestUrl:String = "https://api.github.com/repos/\(reposiroty!.rFullName)/pulls"
        
        Alamofire.request(requestUrl, method: .get, parameters: [:]).responseJSON { (response) in
            if response.result.isFailure {
                print(response.result.error!)
                
            } else {
                if let JSON = response.result.value as? NSArray {
                    
                    for item in JSON {
                        let i = item as! NSDictionary
                        let newItem = RepoItem()
                        newItem.rtitle = i.object(forKey: "title") as! String
                        newItem.rDate = i.object(forKey: "created_at") as! String
                        newItem.rBody = i.object(forKey: "body") as! String
                        newItem.rPullLink = i.object(forKey: "html_url") as! String
                        
                        let user = i.object(forKey: "user") as! NSDictionary
                        newItem.rName = user.object(forKey: "login") as! String
                        newItem.rImage = user.object(forKey: "avatar_url") as! String
                        
                        self.itens.append(newItem)
                    }
                    self.itensTable.reloadData()
                }
            }
        }
    }
    
    //MARK: TableView
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepoItemCell") as! RepoItemCell
        
        let item = itens[indexPath.row]
        cell.itemTitle.text = item.rtitle
        cell.itemDesc.text = item.rBody
        cell.itemAuthorName.text = item.rName
        cell.itemDate.text = item.rDate
        cell.itemAuthorImage.sd_setImage(with: URL(string: item.rImage), placeholderImage: UIImage(named: "placeholder.jpeg"))
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itens.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = itens[indexPath.row]
        UIApplication.shared.openURL(URL(string: item.rPullLink)!)
    }
}
