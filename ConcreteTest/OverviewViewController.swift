//
//  OverviewViewController.swift
//  ConcreteTest
//
//  Created by Breno Morais on 05/07/17.
//  Copyright © 2017 Breno Morais. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class OverviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var rTable: UITableView!
    
    var page = 1
    var repositories:[Repository] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Teste iOS"
        rTable.delegate = self
        rTable.dataSource = self
        rTable.register(UINib.init(nibName: "RepoCell", bundle: Bundle.main), forCellReuseIdentifier: "RepoCell")
        rTable.register(UINib.init(nibName: "LoadingCell", bundle: Bundle.main), forCellReuseIdentifier: "LoadingCell")
        
        loadRepositories(page: page)
    }
    
    func loadRepositories(page:Int){
        let requestUrl:String = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)"
        
        Alamofire.request(requestUrl, method: .get, parameters: [:]).responseJSON { (response) in
            if response.result.isFailure {
                print(response.result.error!)
                
            } else {
                if let JSON = response.result.value as? NSDictionary {
                    if let itens = JSON["items"] as? NSArray {
                        for item in itens {
                            let r = item as! NSDictionary
                            let newRep = Repository()
                            
                            let rId:NSNumber = r.object(forKey: "id") as! NSNumber
                            let rName:String = r.object(forKey: "name") as! String
                            let rDesc:String = r.object(forKey: "description") is NSNull ? "" : r.object(forKey: "description") as! String
                            let rStars:NSNumber = r.object(forKey: "stargazers_count") as! NSNumber
                            let rForks:NSNumber = r.object(forKey: "forks_count") as! NSNumber
                            let rFullName:String = r.object(forKey: "full_name") as! String
                            
                            let owner = r.object(forKey: "owner") as! NSDictionary
                            let rUserName:String = owner.object(forKey: "login") as! String
                            let rImage:String = owner.object(forKey: "avatar_url") as! String
                            
                            newRep.rId = rId
                            newRep.rFullName = rFullName
                            newRep.rName = rName
                            newRep.rDesc = rDesc
                            newRep.rUserName = rUserName
                            newRep.rStars = rStars
                            newRep.rForks = rForks
                            newRep.rImage = rImage
                            
                            self.repositories.append(newRep)
                        }
                        self.page = self.page + 1
                        self.rTable.reloadData()
                    }
                }
            }
        }
    }
    
    //Mark:TableView
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row != repositories.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RepoCell") as! RepoCell
            let r = repositories[indexPath.row]
            cell.rName.text = r.rName
            cell.rDesc.text = r.rDesc
            cell.rStars.text = "Stars: \(r.rStars)"
            cell.rForks.text = "Forks: \(r.rForks)"
            cell.rAuthor.text = r.rUserName
            cell.rAuthorImage.sd_setImage(with: URL(string: r.rImage), placeholderImage: UIImage(named: "placeholder.jpeg"))
            
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell") as! LoadingCell
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row != repositories.count {
            return 145.0
        } else {
            return 100.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RepoViewController") as! RepoViewController
        vc.reposiroty = repositories[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == repositories.count && repositories.count > 0 {
            loadRepositories(page: page)
        }
    }
    
}

