//
//  RepoItemCell.swift
//  ConcreteTest
//
//  Created by Bohm Soluções on 06/07/17.
//  Copyright © 2017 Breno Morais. All rights reserved.
//

import UIKit

class RepoItemCell: UITableViewCell {
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var itemAuthorImage: UIImageView!
    @IBOutlet weak var itemAuthorName: UILabel!
    @IBOutlet weak var itemDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
