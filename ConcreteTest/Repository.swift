//
//  Repository.swift
//  ConcreteTest
//
//  Created by Breno Morais on 05/07/17.
//  Copyright © 2017 Breno Morais. All rights reserved.
//

import UIKit

class Repository: NSObject {
    var rId:NSNumber = 0
    var rFullName:String = ""
    var rName:String = ""
    var rDesc:String = ""
    var rUserName:String = ""
    var rStars:NSNumber = 0
    var rForks:NSNumber = 0
    var rImage:String = ""
}
